class TiGadgetSingleton {
  static final TiGadgetSingleton _singleton = new TiGadgetSingleton._internal();
  TiGadgetSingleton._internal();

  static TiGadgetSingleton get instance => _singleton;

  var somedata;
}
