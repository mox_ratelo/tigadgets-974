import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:intl/intl.dart';

final scaffoldBackgroundGrey = Colors.grey[200];

final scaffoldBackgroundColor = Colors.grey[100];
const backgroundColorDark = Color(0xFF343434);

const double padding = 10;
const double avatarRadius = 30;

double getScreenHeight(BuildContext context) {
  final height = MediaQuery.of(context).size.height;
  return height;
}

double getScreenWidth(BuildContext context) {
  final width = MediaQuery.of(context).size.width;
  return width;
}

String formatDate(DateTime date) {
  var format = DateFormat.yMMMd('fr_FR').format(date);
  return format;
}

int generateNumber() {
  var rnd = new math.Random();
  var next = rnd.nextDouble() * 1000;
  while (next < 10) {
    next *= 10;
  }
  return next.toInt();
}
