import 'package:shared_preferences/shared_preferences.dart';

class SessionManager {
  SessionManager._privateConstructor();

  static final SessionManager instance = SessionManager._privateConstructor();

  SharedPreferences prefs;
  // ignore: empty_constructor_bodies
  //SessionManager() {}

  addStringValueWithKey(String _key, String _value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(_key, _value);
  }

  Future<String> getStringValueWithKey(String _key) async {
    prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString(_key);
    return stringValue ?? null;
  }

  addIntValueWithKey(String _key, int _value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(_key, _value);
  }

  Future<int> getIntValueWithKey(String _key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int intValue = prefs.getInt(_key);
    return intValue ?? 0;
  }

  addDoubleValueWithKey(String _key, double _value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setDouble(_key, _value);
  }

  Future<double> getDoubleValueWithKey(String _key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    double doubleValue = prefs.getDouble(_key);
    return doubleValue ?? 0.0;
  }

  addBooleanValueWithKey(String _key, bool _value) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setBool(_key, _value);
  }

  Future<bool> getBoolValueWithKey(String _key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool boolValue = prefs.getBool(_key);
    return boolValue ?? false;
  }
}
