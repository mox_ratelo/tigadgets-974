import 'package:flutter/material.dart';

abstract class Style {
  ///fontFamily "Raleway"
  static final raleway = "Raleway";
  static final ralewayBold = "ralewayBold";

  ///fontFamily "Montserrat"

  static const primaryColor = Color(0xFF122341);
  static const secondaryColor = Color(0xFFff3505);
  static const backgroundColorGrey = Color(0xFFebeef4);

  static final montserrat = "Montserrat";
  static final montserratBold = "montserratBold";
  static final titleColor = Color(0xFF323c64);
  static final textColorGrey = Style.titleColor.withOpacity(0.6);
  static final shimmerPrimaryColor = Colors.blueGrey[50];
  static final shimmerBackground = Color(0xFFf8f7fc);
  static final boxShadowColor = Color(0xFF3c6dd4).withOpacity(0.15);
  static final containerPadding =
      EdgeInsets.symmetric(horizontal: 50, vertical: 30);
  static final containerPaddingHorizontal = 50.0;
  static final containerPaddingVertical = 30.0;
  static final containerHorizontalMargin = 20.0;
  static final backgroundScaffold = Color(0xFFe7e4f1);
  static final colorGreen = Color(0xFF35e0d0);
  static final gradientButton1 = LinearGradient(colors: [
    Color(0xFFbe67e8),
    Color(0xFF3c75d4),
    Color(0xFF35e0d0),
  ], begin: Alignment.bottomLeft, end: Alignment.topRight);
  static final alertColor = Color(0xFFd83560);
  static final boxShadow = [
    BoxShadow(
        color: Style.boxShadowColor, offset: Offset(0, 10), blurRadius: 10)
  ];
  static final tsork = Image.asset(
    "assets/images/tsork.png",
    scale: 5,
  );

  static const fonts_noirPro_bold = "NoirPro-Bold";
  static const fonts_noirPro_heavy = "NoirPro-Heavy";
  static const fonts_noirPro_italic = "NoirPro-Italic";
  static const fonts_noirPro_light = "NoirPro-Light";
  static const fonts_noirPro_medium = "NoirPro-Medium";
  static const fonts_noirPro_regular = "NoirPro-Regular";
  static const fonts_noirPro_semiBold = "NoirPro-SemiBold";
  static const fonts_grouchBT = "GrouchBT";
  static const appBarTitleStyle = TextStyle(
      color: Style.primaryColor,
      fontSize: 15,
      fontFamily: Style.fonts_noirPro_semiBold,
      fontWeight: FontWeight.bold);
}
