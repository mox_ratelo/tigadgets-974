import 'package:Tigadgets_974/src/utils/globalVariable.dart';
import 'package:Tigadgets_974/src/utils/style.dart';
import 'package:flutter/material.dart';

import 'clipper_path.dart';

class MainMenuCard extends StatelessWidget {
  final String title;
  final Color titleColor;
  final String subTitle;
  final Color subTitleColor;
  final Color backgroundColor;
  final Function onTap;
  const MainMenuCard(
      {Key key,
      this.title,
      this.titleColor,
      this.subTitle,
      this.subTitleColor,
      this.backgroundColor,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 220,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            color: backgroundColor ?? Style.secondaryColor),
        child: FlatButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
          padding: EdgeInsets.all(0),
          onPressed: onTap,
          child: Stack(
            children: [
              Positioned(
                  top: 0,
                  bottom: 0,
                  left: 40,
                  child: Container(
                      height: 200,
                      width: getScreenWidth(context) * .5,
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              title,
                              style: TextStyle(
                                  color: titleColor ?? Colors.white,
                                  fontFamily: Style.fonts_noirPro_medium,
                                  fontSize: 24),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Text(
                              subTitle,
                              style: TextStyle(
                                  color: subTitleColor ?? Colors.white,
                                  fontSize: 17),
                            ),
                          ],
                        ),
                      ))),
              Positioned(
                right: 30,
                top: 30,
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [
                        BoxShadow(
                            offset: Offset(0, 10),
                            blurRadius: 10,
                            color: Style.primaryColor.withOpacity(0.20))
                      ]),
                  width: 60,
                  height: 60,
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Icon(
                      Icons.arrow_forward_ios,
                      size: 18,
                      color: backgroundColor != null
                          ? Style.primaryColor
                          : Style.secondaryColor ?? Style.secondaryColor,
                    ),
                    onPressed: () {},
                  ),
                ),
              ),
              Positioned(
                right: 0,
                child: ClipPath(
                  clipper: ClipperPath(),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.only(topRight: Radius.circular(30)),
                      color: Colors.black.withOpacity(0.09),
                    ),
                    width: 250,
                    height: 250,
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
