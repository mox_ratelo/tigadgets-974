import 'package:Tigadgets_974/src/utils/globalVariable.dart';
import 'package:flutter/material.dart';

class DialogContainer extends StatelessWidget {
  final Widget child;
  const DialogContainer({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        scrollable: true,
        elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        content: Container(
          child: child,
          width: getScreenWidth(context) * .8,
        ));
  }
}
