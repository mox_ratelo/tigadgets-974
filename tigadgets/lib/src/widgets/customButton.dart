import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final Color textColor;
  final Color color;
  final double elevation;
  final Function onPressed;
  const CustomButton({Key key, @required this.text, @required this.onPressed, this.elevation, this.color, this.textColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: double.infinity,
      child: RaisedButton(
        elevation: elevation ?? 0,
        color: color ?? Theme.of(context).primaryColor,
        child: Text(text, style: TextStyle(color: textColor ?? Colors.white),),
        onPressed: onPressed 
      ),
    );
  }
}