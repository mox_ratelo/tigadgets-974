import 'package:Tigadgets_974/src/utils/style.dart';
import 'package:flutter/material.dart';

class FooterLogo extends StatelessWidget {
  final Color color;
  const FooterLogo({Key key, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90,
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset("assets/images/footer-logo.png",
              scale: 2.5, color: color ?? Style.primaryColor),
          SizedBox(
            height: 10,
          ),
          Text(
            "www.love-health-center.org",
            style: TextStyle(fontSize: 13, color: color ?? Style.primaryColor),
          )
        ],
      ),
    );
  }
}
