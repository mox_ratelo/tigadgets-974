import 'package:flutter/services.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  Future exit() async {
    return SystemNavigator.pop();
  }
}
