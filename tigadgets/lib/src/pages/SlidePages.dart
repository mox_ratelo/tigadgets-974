import 'package:Tigadgets_974/src/pages/Slide/Slide1.dart';
import 'package:Tigadgets_974/src/pages/Slide/Slide3.dart';
import 'package:Tigadgets_974/src/utils/globalVariable.dart';
import 'package:Tigadgets_974/src/widgets/carouselSlider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Slide/Slide2.dart';

class SlidePages extends StatefulWidget {
  @override
  _SlidePageState createState() => _SlidePageState();
}

class _SlidePageState extends State<SlidePages> {
  int _currentIndex = 0;
  PageController _pageController = PageController();
  ValueNotifier<double> _valueNotifier = ValueNotifier(0.0);

  @override
  void dispose() {
    _pageController.dispose();
    _valueNotifier.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: scaffoldBackgroundGrey,
      body: Stack(
        children: <Widget>[
          GestureDetector(
              child: Center(
                child: CarouselSlider(
                  pageController: _pageController,
                  notifier: _valueNotifier,
                  scrollPhysics: NeverScrollableScrollPhysics(),
                  viewportFraction: 1.0,
                  enableInfiniteScroll: false,
                  height: getScreenHeight(context),
                  items: <Widget>[
                    Slide1(context, _pageController),
                    Slide2(context, _pageController),
                    Slide3(context, _pageController),
                  ],
                  onPageChanged: (index) {
                    setState(() {
                      _currentIndex = index;
                    });
                  },
                ),
              ),
              onPanUpdate: (details) {
                if (details.delta.dx > 0) {
                  _pageController.previousPage(
                      duration: Duration(milliseconds: 600),
                      curve: Curves.fastOutSlowIn);
                } else {
                  _pageController.nextPage(
                      duration: Duration(milliseconds: 600),
                      curve: Curves.fastOutSlowIn);
                }
              }),
          Positioned(
            bottom: 0,
            child: Container(
              height: 50,
              width: getScreenWidth(context),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: getScreenWidth(context) / 2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: map<Widget>(
                        slideItem,
                        (index, url) {
                          return Container(
                            width: _currentIndex == index ? 20.0 : 8.0,
                            height: _currentIndex == index ? 8.0 : 8.0,
                            margin: EdgeInsets.symmetric(
                                vertical: 5.0, horizontal: 2.0),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color: _currentIndex == index
                                    ? Colors.blueAccent
                                    : Colors.grey),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  int get newMethod => 0xFF35d8d0;
}

class SlideItem {
  RichText title;
  String image;
  String description;
  SlideItem(this.title, this.image, this.description);
}

final List<SlideItem> slideItem = [
  SlideItem(
      RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          text: " Avec ",
          children: <TextSpan>[
            tigadgetIoStyle,
            TextSpan(
                text: ', localisez facilement les bacs à ordure à proximité.')
          ],
        ),
      ),
      'assets/images/imgVectorSlide1.png',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabi Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabi Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabi Lorem ipsum dolor sit amet,'),
  SlideItem(
      RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          text: " Connectez-vous pour utiliser ",
          children: <TextSpan>[
            tigadgetIoStyle,
            TextSpan(text: ' plus aisément.')
          ],
        ),
      ),
      'assets/images/imgVectorSlide3.png',
      'Lorem ipsum dolor sit amet, Curabi Lorem ipsum dolor sit amet,'),
  SlideItem(
      RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          style: TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          text: " Connectez-vous pour utiliser ",
          children: <TextSpan>[
            tigadgetIoStyle,
            TextSpan(text: ' plus aisément.')
          ],
        ),
      ),
      'assets/images/imgVectorSlide3.png',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabi Lorem ipsum dolor sit amet,'),
];

const tigadgetIoStyle = TextSpan(
    style: TextStyle(color: const Color(0xFF03B4F6)),
    text: 'TiGadget',
    children: [
      TextSpan(
          text: '.974',
          style: TextStyle(
            color: const Color(0xFF75D21F),
          )),
    ]);

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }
  return result;
}
