import 'package:Tigadgets_974/src/pages/Tabs/DetailArticle.dart';
import 'package:Tigadgets_974/src/utils/globalVariable.dart';
import 'package:Tigadgets_974/src/utils/style.dart';
import 'package:Tigadgets_974/src/widgets/primary_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RoleChoiceCard extends StatelessWidget {
  final EdgeInsets margin;
  final String roleTitle;
  final String img;
  final Function onTap;
  const RoleChoiceCard({
    Key key,
    this.roleTitle,
    this.img,
    this.margin,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 35, vertical: 40),
      margin: margin ?? EdgeInsets.only(right: 40),
      width: getScreenWidth(context) * .8,
      decoration: BoxDecoration(
          color: Color(0xFFebeef4), borderRadius: BorderRadius.circular(30)),
      child: Column(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Text("iPhone 12".tr,
                style: TextStyle(color: Color(0xFF1f4488), fontSize: 28)),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Text("Courte description ...",
                style: TextStyle(
                    fontFamily: Style.fonts_grouchBT,
                    color: Style.primaryColor,
                    fontSize: 10)),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            height: getScreenHeight(context) * .32,
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(img) ??
                        AssetImage("assets/images/evras_imgBack.png"),
                    fit: BoxFit.contain)),
          ),
          PrimaryButton(
              width: 120,
              height: 35,
              child: Text("Voir détail".tr,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontFamily: Style.fonts_noirPro_medium)),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetailArticle(),
                  ),
                );
              }),
        ],
      ),
    );
  }
}
