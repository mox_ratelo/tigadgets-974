import 'package:Tigadgets_974/modeles/chat.dart';
import 'package:Tigadgets_974/src/pages/Tabs/Messages/messageCard.dart';
import 'package:Tigadgets_974/src/utils/globalVariable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class MessageTab extends StatefulWidget {
  @override
  _MessageTabState createState() => _MessageTabState();
}

class _MessageTabState extends State<MessageTab>
    with AutomaticKeepAliveClientMixin<MessageTab> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;
  TextEditingController textEditingController = TextEditingController();
  bool showInputOption = false;

  @override
  void initState() {
    super.initState();
    print('initState Tab2');
  }

  @override
  Widget build(BuildContext context) {
    print('build Tab2');
    return Scaffold(
      appBar: AppBar(title: Text('Message'), backgroundColor: Colors.black),
      /*floatingActionButton: AnimatedOpacity(
        child: FloatingActionButton(
          child: Icon(Icons.add),
          tooltip: "Ajouter une article",
          backgroundColor: Colors.red,
          onPressed: () {},
        ),
        duration: Duration(milliseconds: 100),
        opacity: 1, //Set 0 to hide floatingActionButton
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,*/
      body: Stack(
        children: <Widget>[
          Container(
              width: double.infinity,
              color: backgroundColorDark,
              padding: EdgeInsets.only(bottom: 5),
              height: MediaQuery.of(context).size.height * .4,
              child: Column(
                children: <Widget>[
                  Container(
                    width: 80,
                    height: 80,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: Center(
                        child: Image.asset(
                      "assets/images/logo_g.png",
                      scale: 6,
                    )),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width * .5,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Tigadgets",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontSize: 20),
                            textAlign: TextAlign.center,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: 8,
                                height: 8,
                                decoration: BoxDecoration(
                                    color: Colors.lightGreen,
                                    borderRadius: BorderRadius.circular(30)),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                "Connecté",
                                style: TextStyle(
                                    color: Colors.white60, fontSize: 14),
                              ),
                            ],
                          )
                        ],
                      ))
                ],
              )),
          DraggableScrollableSheet(
              minChildSize: 0.7,
              maxChildSize: 1.0,
              initialChildSize: 0.7,
              builder: (context, scrollController) {
                return Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10))),
                    child: Column(
                      children: <Widget>[
                        // SizedBox(height: 20,),
                        // Container(
                        //   width: double.infinity,
                        //   height: 200,
                        //   child: _image != null ? Image.file(_image, fit: BoxFit.contain) : Container(),
                        // ),
                        //List of messages
                        _buildListMessage(scrollController),

                        //Input content
                        _buildInput(),
                      ],
                    ));
              })
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  Widget _buildListMessage(scrollController) {
    List<Chat> _chats = [
      Chat(
          message: "bonjour bonjour",
          userId: '4434fsdfkl',
          userName: "Selena Gomez"),
      Chat(message: "Salut", userId: 'mandresyId12', userName: "Mandresy"),
      Chat(message: "Oui", userId: 'mandresyId12', userName: "Mandresy"),
    ].reversed.toList();
    return Flexible(
      child: ListView.builder(
          physics: BouncingScrollPhysics(),
          itemCount: _chats.length,
          controller: scrollController,
          itemBuilder: (context, position) {
            if (position == 0) {
              return SizedBox(
                height: 10,
              );
            } else {
              return Container(
                  padding: EdgeInsets.only(left: 15, right: 15),
                  child: MessageCard(
                    chat: _chats[position],
                    position: position,
                  ));
            }
          }),
    );
  }

  void _onSendMessage() {
    if (textEditingController.text.trim() != '') {
      textEditingController.text = "";
    } else {
      print('Nothing to send');
    }
  }

  Widget _buildInput() {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
        padding: EdgeInsets.symmetric(horizontal: 5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30), color: Colors.grey[100]),
        child: Row(
          children: <Widget>[
            if (!showInputOption)
              Container(
                margin: EdgeInsets.all(8),
                height: 30,
                width: 30,
                decoration: BoxDecoration(
                    color: backgroundColorDark,
                    borderRadius: BorderRadius.circular(30)),
                child: CupertinoButton(
                    padding: EdgeInsets.all(0),
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                      size: 18,
                    ),
                    onPressed: () {
                      setState(() {
                        showInputOption = true;
                      });
                    }),
              )
            else
              Container(
                  margin: EdgeInsets.all(8),
                  height: 30,
                  decoration: BoxDecoration(
                      color: backgroundColorDark,
                      borderRadius: BorderRadius.circular(30)),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      CupertinoButton(
                          padding: EdgeInsets.all(0),
                          child: Icon(
                            Icons.camera_alt,
                            color: Colors.white,
                            size: 18,
                          ),
                          onPressed: () {
                            //_getimageditor();
                          }),
                      CupertinoButton(
                          padding: EdgeInsets.all(0),
                          child: Icon(
                            Icons.attachment,
                            color: Colors.white,
                            size: 18,
                          ),
                          onPressed: () {}),
                      CupertinoButton(
                          padding: EdgeInsets.all(0),
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                            size: 18,
                          ),
                          onPressed: () {
                            setState(() {
                              showInputOption = false;
                            });
                          }),
                    ],
                  )),
            Flexible(
              child: TextField(
                scrollPhysics: BouncingScrollPhysics(),
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 3,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(vertical: 15),
                    hintText: "Votre message ici",
                    labelStyle: TextStyle(
                        color: Colors.grey[800],
                        fontSize: 14,
                        fontWeight: FontWeight.bold),
                    border: InputBorder.none,
                    suffixIcon: CupertinoButton(
                        padding: EdgeInsets.all(0),
                        child: Image.asset(
                          "assets/images/envoi.png",
                          scale: 3,
                        ),
                        onPressed: () {
                          _onSendMessage();
                        })),
              ),
            )
          ],
        ));
  }
}
