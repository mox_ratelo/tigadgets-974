import 'package:Tigadgets_974/src/utils/globalVariable.dart';
import 'package:flutter/material.dart';

class DetailArticle extends StatefulWidget {
  @override
  _DetailArticleState createState() => _DetailArticleState();
}

class _DetailArticleState extends State<DetailArticle>
    with AutomaticKeepAliveClientMixin<DetailArticle> {
  @override
  void initState() {
    super.initState();
    print('initState Tab2');
  }

  @override
  Widget build(BuildContext context) {
    print('Détail Article');
    return Scaffold(
      appBar: AppBar(
        title: Text('Artiles'),
        backgroundColor: Colors.black,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.add_box,
              color: Colors.white,
            ),
            onPressed: () {
              // do something
            },
          ),
          IconButton(
            icon: Icon(
              Icons.details,
              color: Colors.white,
            ),
            onPressed: () {
              // do something
            },
          )
        ],
      ),
      body: SingleChildScrollView(
          //width: getScreenHeight(context) * .5,
          padding: EdgeInsets.all(5.0),
          //margin: const EdgeInsets.only(left: 3.0, right: 3.0),
          physics: BouncingScrollPhysics(),
          child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 8,
                ),
                new Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("iPhone 12",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Color(0xFF1f4488),
                          fontSize: 32,
                        )),
                    SizedBox(
                      width: 8,
                    ),
                    Expanded(
                        child: Text(
                      "850€",
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                      ),
                    )),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  //margin: EdgeInsets.symmetric(vertical: 5),
                  height: getScreenHeight(context) * .5,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/img_iphone2.png"),
                          fit: BoxFit.contain)),
                ),
                SizedBox(
                  height: 10,
                ),
                Text("Fiche technique :",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 22,
                      decoration: TextDecoration.underline,
                      decorationStyle: TextDecorationStyle.double,
                      fontWeight: FontWeight.bold,
                    )),
                SizedBox(
                  height: 8,
                ),
                new Text(
                  "L'iPhone 12 est le modèle principal de la 14e génération de smartphone d'Apple annoncé le 13 octobre 2020. Il est équipé d'un écran de 6,1 pouces OLED HDR 60 Hz, d'un double capteur photo avec ultra grand-angle et d'un SoC Apple A14 Bionic compatible 5G (sub-6 GHz)." +
                      "L'iPhone 12 est le modèle principal de la 14e génération de smartphone d'Apple annoncé le 13 octobre 2020. Il est équipé d'un écran de 6,1 pouces OLED HDR 60 Hz, d'un double capteur photo avec ultra grand-angle et d'un SoC Apple A14 Bionic compatible 5G (sub-6 GHz)." +
                      "L'iPhone 12 est le modèle principal de la 14e génération de smartphone d'Apple annoncé le 13 octobre 2020. Il est équipé d'un écran de 6,1 pouces OLED HDR 60 Hz, d'un double capteur photo avec ultra grand-angle et d'un SoC Apple A14 Bionic compatible 5G (sub-6 GHz).",
                  style: new TextStyle(
                    fontSize: 16.0,
                    color: Colors.black38,
                  ),
                ),
                //END
              ])),
      /*body: new Container(
        width: getScreenHeight(context) * .5,
        padding: EdgeInsets.all(5.0),
        margin: const EdgeInsets.only(left: 3.0, right: 3.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 8,
            ),
            new Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("iPhone 12",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Color(0xFF1f4488),
                      fontSize: 32,
                    )),
                SizedBox(
                  width: 8,
                ),
                Expanded(
                    child: Text(
                  "850€",
                  textDirection: TextDirection.ltr,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 32,
                  ),
                )),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              //margin: EdgeInsets.symmetric(vertical: 5),
              height: getScreenHeight(context) * .5,
              width: double.infinity,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/img_iphone2.png"),
                      fit: BoxFit.contain)),
            ),
            SizedBox(
              height: 10,
            ),
            Text("Fiche technique :",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 22,
                  decoration: TextDecoration.underline,
                  decorationStyle: TextDecorationStyle.double,
                  fontWeight: FontWeight.bold,
                )),
            SizedBox(
              height: 8,
            ),
            new Expanded(
              flex: 1,
              child: new SingleChildScrollView(
                scrollDirection: Axis.vertical, //.horizontal
                child: new Text(
                  "L'iPhone 12 est le modèle principal de la 14e génération de smartphone d'Apple annoncé le 13 octobre 2020. Il est équipé d'un écran de 6,1 pouces OLED HDR 60 Hz, d'un double capteur photo avec ultra grand-angle et d'un SoC Apple A14 Bionic compatible 5G (sub-6 GHz)." +
                      "L'iPhone 12 est le modèle principal de la 14e génération de smartphone d'Apple annoncé le 13 octobre 2020. Il est équipé d'un écran de 6,1 pouces OLED HDR 60 Hz, d'un double capteur photo avec ultra grand-angle et d'un SoC Apple A14 Bionic compatible 5G (sub-6 GHz)." +
                      "L'iPhone 12 est le modèle principal de la 14e génération de smartphone d'Apple annoncé le 13 octobre 2020. Il est équipé d'un écran de 6,1 pouces OLED HDR 60 Hz, d'un double capteur photo avec ultra grand-angle et d'un SoC Apple A14 Bionic compatible 5G (sub-6 GHz).",
                  style: new TextStyle(
                    fontSize: 16.0,
                    color: Colors.black38,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),*/
    );
  }

  @override
  bool get wantKeepAlive => true;
}
