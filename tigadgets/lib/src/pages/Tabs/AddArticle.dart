import 'dart:ui';
import 'package:Tigadgets_974/src/utils/globalVariable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class AddArticle extends StatefulWidget {
  final String title, descriptions, text;
  final Image img;

  const AddArticle(
      {Key key, this.title, this.descriptions, this.text, this.img})
      : super(key: key);

  @override
  _AddArticleState createState() => _AddArticleState();
}

class _AddArticleState extends State<AddArticle> {
  File _image;
  final picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    //_image
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(padding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
      insetPadding: EdgeInsets.symmetric(horizontal: 10),
    );
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      }
    });
  }

  contentBox(context) {
    String _selectedFilter;
    TextEditingController _quatiteController = new TextEditingController();
    GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();

    return Stack(
      children: <Widget>[
        Container(
          padding: MediaQuery.of(context).viewInsets +
              const EdgeInsets.symmetric(horizontal: 8.0, vertical: 20.0),
          width: MediaQuery.of(context).size.width - 10,
          height: MediaQuery.of(context).size.height - 100,

          /*padding: EdgeInsets.only(
              left: padding,
              top: avatarRadius + padding,
              right: padding,
              bottom: padding),*/
          //margin: EdgeInsets.only(top: avatarRadius),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(padding),
              boxShadow: [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10),
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        width: 80,
                        height: 80,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            image: DecorationImage(
                                image: _image == null
                                    ? AssetImage("assets/images/img_pic.png")
                                    : FileImage(_image),
                                fit: BoxFit.cover)),
                      ),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        width: 150,
                        child: RaisedButton(
                          color: Colors.white,
                          elevation: 1,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          child: Text("Modifier la photo"),
                          onPressed: () async {
                            await getImage();
                          },
                        ),
                      ),
                    ),
                  ]),
              Container(
                margin: EdgeInsets.symmetric(vertical: 0),
                padding: EdgeInsets.symmetric(horizontal: 1),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.grey[100]),
                child: Card(
                    margin: EdgeInsets.symmetric(vertical: 0),
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 1),
                      child: SimpleAutoCompleteTextField(
                        key: key,
                        suggestions: [
                          "Apple",
                          "Pochettes",
                          "Coques",
                          "Ecouteurs",
                          "Câble",
                          "Gadgets",
                        ],
                        decoration: InputDecoration(
                            labelText: "Catégorie",
                            filled: true,
                            //fillColor: Colors.black12,
                            labelStyle: TextStyle(
                                color: Colors.grey[800],
                                fontSize: 14,
                                fontWeight: FontWeight.bold)),
                      ),

                      /*DropdownButtonFormField<String>(
                        value: _selectedFilter,
                        items: [
                          "Pochettes",
                          "Coques",
                          "Ecouteurs",
                          "Câbles",
                          "Gadgets"
                        ]
                            .map((label) => DropdownMenuItem(
                                  child: Text(label.toString()),
                                  value: label,
                                ))
                            .toList(),
                        hint: Text('Catégorie'),
                        onChanged: (value) {
                          setState(() {
                            _selectedFilter = value;
                          });
                        },
                      ),*/
                    )),
              ),
              SizedBox(
                height: 3,
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 5),
                padding: EdgeInsets.symmetric(horizontal: 15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.grey[100]),
                child: TextField(
                  decoration: InputDecoration(
                      labelText: "Nom de l'article",
                      labelStyle: TextStyle(
                          color: Colors.grey[800],
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                      border: InputBorder.none),
                ),
              ),
              SizedBox(
                height: 3,
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 5),
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.grey[100]),
                      child: TextField(
                        controller: _quatiteController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelText: "Quantité",
                            labelStyle: TextStyle(
                                color: Colors.grey[800],
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                            border: InputBorder.none),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 0,
                    child: Container(
                      width: 5,
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 5),
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.grey[100]),
                      child: TextField(
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelText: "Prix",
                            labelStyle: TextStyle(
                                color: Colors.grey[800],
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                            border: InputBorder.none),
                      ),
                    ),
                  ),
                  /*new Flexible(
                    child: new TextField(
                      decoration: InputDecoration(
                          labelText: "Prix",
                          labelStyle: TextStyle(
                              color: Colors.grey[800],
                              fontSize: 14,
                              fontWeight: FontWeight.bold),
                          border: InputBorder.none),
                    ),
                  ),*/
                ],
              ),
              /*Container(
                margin: EdgeInsets.symmetric(vertical: 5),
                padding: EdgeInsets.symmetric(horizontal: 15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.grey[100]),
                child: TextField(
                  decoration: InputDecoration(
                      labelText: "Prix",
                      labelStyle: TextStyle(
                          color: Colors.grey[800],
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                      border: InputBorder.none),
                ),
              ),*/
              SizedBox(
                height: 3,
              ),
              Card(
                  color: Colors.grey[100],
                  child: Padding(
                    padding: EdgeInsets.all(0.0),
                    child: TextField(
                      maxLines: 8,
                      decoration: InputDecoration(
                        labelText: "Description de l'article",
                        labelStyle: TextStyle(
                            color: Colors.grey[800],
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                        //hintText: "Description de l'article"
                      ),
                    ),
                  )),
              SizedBox(
                height: 22,
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Container(
                  width: 150,
                  child: RaisedButton(
                    color: Colors.white,
                    elevation: 1,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                    child: Text(
                      "Ajouter",
                      style: TextStyle(fontSize: 18),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
        /*Positioned(
          left: padding,
          right: padding,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: avatarRadius,
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(avatarRadius)),
                child: Image.asset("assets/images/logo_g.png")),
          ),
        ),*/
      ],
    );
  }
}
