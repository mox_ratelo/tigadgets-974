import 'package:Tigadgets_974/src/utils/TiGadgetSingleton.dart';
import 'package:flutter/material.dart';

class AProposTab extends StatefulWidget {
  @override
  _AProposTabState createState() => _AProposTabState();
}

class _AProposTabState extends State<AProposTab>
    with AutomaticKeepAliveClientMixin<AProposTab> {
  @override
  void initState() {
    super.initState();
    print('initState Tab3');
  }

  @override
  Widget build(BuildContext context) {
    print('build Tab3');

    return Scaffold(
      appBar: AppBar(
        title: Text('A propos'),
        backgroundColor: Colors.black,
      ),
      body: Center(
        child: Text(
          '',
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
