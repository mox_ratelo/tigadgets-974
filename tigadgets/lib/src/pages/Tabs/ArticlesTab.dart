import 'package:Tigadgets_974/src/pages/Tabs/AddArticle.dart';
import 'package:Tigadgets_974/src/pages/Tabs/DetailArticle.dart';
import 'package:Tigadgets_974/src/pages/Tabs/role_choice_card.dart';
import 'package:Tigadgets_974/src/utils/TiGadgetSingleton.dart';
import 'package:Tigadgets_974/src/utils/globalVariable.dart';
import 'package:Tigadgets_974/src/utils/style.dart';
import 'package:flutter/material.dart';

class ArticleTab extends StatefulWidget {
  @override
  _ArticleTabState createState() => _ArticleTabState();
}

class _ArticleTabState extends State<ArticleTab>
    with AutomaticKeepAliveClientMixin<ArticleTab> {
  @override
  void initState() {
    super.initState();
    print('initState Tab1');
  }

  @override
  Widget build(BuildContext context) {
    print('build Tab1');

    final listOfArticles = List<Widget>();

    String _selectedFilter;

    for (var i = 0; i < 10; i++) {
      listOfArticles.add(RoleChoiceCard(
        roleTitle: null,
        img: "assets/images/img_iphone2.png",
        margin: i > 0
            ? EdgeInsets.only(right: 40)
            : EdgeInsets.only(right: 20, left: 20),
        onTap: () {},
      ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Articles'),
        backgroundColor: Colors.black,
        /*actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {
              // do something
            },
          )
        ],*/
      ),
      floatingActionButton: AnimatedOpacity(
        child: FloatingActionButton(
          child: Icon(Icons.add),
          tooltip: "Ajouter une article",
          backgroundColor: Colors.red,
          onPressed: () {
            showDialog(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) {
                  return AddArticle(
                    title: "Custom Dialog Demo",
                    descriptions: "",
                    text: "Yes",
                  );
                });
          },
        ),
        duration: Duration(milliseconds: 100),
        opacity: 1, //Set 0 to hide floatingActionButton
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Card(
                margin: EdgeInsets.symmetric(vertical: 5),
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: DropdownButtonFormField<String>(
                    value: _selectedFilter,
                    items: [
                      "Pochettes",
                      "Coques",
                      "Ecouteurs",
                      "Câbles",
                      "Gadgets"
                    ]
                        .map((label) => DropdownMenuItem(
                              child: Text(label.toString()),
                              value: label,
                            ))
                        .toList(),
                    hint: Text('Catégorie'),
                    onChanged: (value) {
                      setState(() {
                        _selectedFilter = value;
                      });
                    },
                  ),
                )),

            /*Container(
              padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
              margin: EdgeInsets.only(right: 60),
              alignment: Alignment.topLeft,
              child: Text(
                "Choisissez \nvotre Gadgets.",
                style: TextStyle(
                    fontFamily: Style.fonts_grouchBT,
                    color: Style.primaryColor,
                    fontSize: 25),
              ),
            ),*/
            SizedBox(
              height: 10,
            ),
            Container(
              height: (getScreenHeight(context) * .6) + .5,
              width: double.infinity,
              alignment: Alignment.topLeft,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children:
                    listOfArticles, /*<Widget>[
                  RoleChoiceCard(
                    roleTitle: "PARTICIPANT(E) ?",
                    img: "assets/images/evras_imgBack.png",
                    margin: EdgeInsets.only(right: 20, left: 20),
                    onTap: () {
                      //Get.toNamed(Routes.PARTICIPANT_JOIN_A_SESSION);
                    },
                  ),
                  RoleChoiceCard(
                    roleTitle: "ANIMATEUR ?",
                    img: "assets/images/evras_imgBack1.png",
                    margin: EdgeInsets.only(right: 40),
                    onTap: () {
                      //controller.isLoggedIn();
                    },
                  ),
                ],*/
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
