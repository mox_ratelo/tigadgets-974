import 'package:Tigadgets_974/modeles/chat.dart';
import 'package:flutter/material.dart';

class MessageCard extends StatelessWidget {
  MessageCard({this.chat, this.position});

  Chat chat;

  double margin;
  bool _isRigth = true;
  int position;
  String currentUserId = 'mandresyId';
  BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context;
    margin = MediaQuery.of(context).size.width / 3;
    //if (user.)
    // _positionMessage();
    return FutureBuilder(
      future: _positionMessage(),
      builder: (ctx, snapshot) {
        if (snapshot.hasData)
          return _body(isRigth: snapshot.data);
        else
          return Container(
              width: 10,
              height: 10,
              child: Center(
                child: CircularProgressIndicator(),
              ));
      },
    );
  }

  Future<bool> _positionMessage() async {
    // String udid = await FlutterUdid.udid;
    // String id = (getHiveUser() != null ? getHiveUser().id.toString() : udid);
    String id;
    await Future.delayed(Duration(milliseconds: 100));
    id = "mandresyId";
    if (chat.userId != id)
      _isRigth = false;
    else
      _isRigth = true;
    return _isRigth;
  }

  _body({bool isRigth}) => Container(
        width: double.infinity,
        /*margin: isRigth
        ? EdgeInsets.only(left: margin)
        : EdgeInsets.only(right: margin),*/
        child: Row(
          mainAxisAlignment:
              isRigth ? MainAxisAlignment.end : MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            isRigth
                ? Container()
                : Stack(
                    children: <Widget>[
                      Container(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: Center(
                            child: Image.asset(
                          "assets/images/logo_g.png",
                          scale: 20,
                        )),
                      ),
                      Positioned(
                        bottom: 0,
                        right: 2,
                        child: Container(
                          width: 7,
                          height: 7,
                          decoration: BoxDecoration(
                              color: Colors.lightGreen,
                              borderRadius: BorderRadius.circular(30)),
                        ),
                      )
                    ],
                  ),
            Flexible(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 10, left: 5),
                  decoration: BoxDecoration(
                    borderRadius: isRigth
                        ? BorderRadius.only(
                            topLeft: Radius.circular(5),
                            topRight: Radius.circular(5),
                            bottomLeft: Radius.circular(5))
                        : BorderRadius.only(
                            topLeft: Radius.circular(5),
                            topRight: Radius.circular(5),
                            bottomRight: Radius.circular(5)),
                    /*gradient: isRigth 
                  ? LinearGradient(colors: [
                      Colors.black,
                      Theme.of(_context).primaryColor
                    ])
                  : LinearGradient(colors: [
                      Colors.grey[200],
                      Colors.grey[200],
                    ])*/
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(15),
                    child: Text(
                      chat.message,
                      style: TextStyle(
                          color: isRigth ? Colors.white : Colors.black),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 5, top: 5),
                  child: Text("20:00",
                      style: TextStyle(
                          color: Colors.grey[500],
                          fontSize: 12,
                          fontWeight: FontWeight.w600)),
                )
              ],
            ))
          ],
        ),
      );
}
