import 'package:Tigadgets_974/src/pages/Tabs/AProposTab.dart';
import 'package:Tigadgets_974/src/pages/Tabs/ArticlesTab.dart';
import 'package:Tigadgets_974/src/pages/Tabs/MessageTab.dart';
import 'package:Tigadgets_974/src/utils/SessionManager.dart';
import 'package:flutter/material.dart';

import '../constants.dart';

class TiGadgetsHomePage extends StatefulWidget {
  TiGadgetsHomePage({Key key}) : super(key: key);

  @override
  _TabContainerDefaultState createState() => _TabContainerDefaultState();
}

class _TabContainerDefaultState extends State<TiGadgetsHomePage> {
  List<Widget> listScreens;
  @override
  void initState() {
    super.initState();
    listScreens = [
      ArticleTab(),
      MessageTab(),
      AProposTab(),
    ];

    SessionManager.instance.addBooleanValueWithKey(PREF_SLIDE_OPEN, true);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: Colors.yellow,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          body: TabBarView(
              physics: NeverScrollableScrollPhysics(), children: listScreens),
          bottomNavigationBar: TabBar(
            tabs: [
              Tab(
                text: 'Articles',
                icon: Icon(Icons.list),
              ),
              Tab(
                text: 'Messages',
                icon: Icon(Icons.message),
              ),
              Tab(
                text: 'A propos',
                icon: Icon(Icons.contact_support),
              ),
            ],
          ),
          backgroundColor: Colors.black,
        ),
      ),
    );
  }
}
