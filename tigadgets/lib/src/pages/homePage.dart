import 'package:Tigadgets_974/src/home/home_controller.dart';
import 'package:Tigadgets_974/src/utils/style.dart';
import 'package:Tigadgets_974/src/widgets/alert_popup.dart';
import 'package:Tigadgets_974/src/widgets/footer_logo.dart';
import 'package:Tigadgets_974/src/widgets/main_menu_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePage extends GetView<HomeController> {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => controller.exit(),
      child: Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                width: double.infinity,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(bottom: 15),
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Bienvenue sur\nl'application du\nLove Health Center."
                            .tr,
                        style: TextStyle(
                            fontFamily: Style.fonts_grouchBT,
                            color: Style.primaryColor,
                            fontSize: 40),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    MainMenuCard(
                      title: "TIRER \nUNE CARTE".tr,
                      subTitle: "Choisissez un thème et répondez aux questions",
                      onTap: () {
                        Get.dialog(AlertPopup(
                            isError: true,
                            title: 'Information',
                            content: "Disponible prochainement.",
                            onCanceled: () => Get.back()));
                        // Get.toNamed(Routes.THEME_CHOICE);
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    MainMenuCard(
                      title: "QUESTIONNAIRE \nE.V.R.A.S.".tr,
                      titleColor: Style.primaryColor,
                      subTitle:
                          "(E.V.R.A.S. - Education à la Vie Relationnelle Affective et Sexuelle) \nSéances avec un animateur.e"
                              .tr,
                      subTitleColor: Style.primaryColor,
                      backgroundColor: Color(0xFFebeef4),
                      onTap: () {
                        Get.dialog(AlertPopup(
                            isError: true,
                            title: 'Information',
                            content: "Disponible prochainement.",
                            onCanceled: () => Get.back()));
                        // Get.toNamed(Routes.THEME_CHOICE);
                      },
                    ),
                  ],
                )),
          ),
          bottomNavigationBar: FooterLogo(),
          endDrawer: null),
    );
  }
}
