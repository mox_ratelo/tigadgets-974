import 'package:Tigadgets_974/src/pages/TiGadgetsHomePage.dart';
import 'package:Tigadgets_974/src/pages/homePage.dart';
import 'package:Tigadgets_974/src/utils/SessionManager.dart';
import 'package:Tigadgets_974/src/utils/globalVariable.dart';
import 'package:Tigadgets_974/src/utils/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

Slide3(BuildContext context, PageController _pageController) {
  return Stack(
    children: <Widget>[
      Positioned(
        top: -50,
        left: 0,
        right: 0,
        child: Container(
          width: double.infinity,
          height: getScreenHeight(context) * .6,
          decoration: BoxDecoration(
              //color: Colors.white,
              image: DecorationImage(
                  image: AssetImage("assets/images/splash3Background.png"),
                  fit: BoxFit.fill)),
        ),
      ),
      Positioned(
        bottom: getScreenHeight(context) * .25,
        right: 0,
        left: 0,
        child: Container(
            margin: EdgeInsets.symmetric(horizontal: 28),
            height: getScreenHeight(context) * .3,
            child: Column(
              children: <Widget>[
                Text(
                  "Retrouvez-nous sur notre Stand d’Accessoires Mobiles Tigadgets 974",
                  style: TextStyle(
                    fontSize: 30,
                    fontFamily: Style.montserratBold,
                    color: Style.titleColor,
                  ),
                  textAlign: TextAlign.center,
                  /*strutStyle: StrutStyle(height: 2.5)*/
                ),
                SizedBox(
                  height: 0,
                ),
              ],
            )),
      ),
      Positioned(
          bottom: getScreenHeight(context) * .1,
          left: 0,
          right: 0,
          child: Container(
            width: double.infinity,
            child: Center(
              child: Container(
                  width: 150,
                  height: 50,
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      color: Colors.red,
                      elevation: 0,
                      onPressed: () {
                        // Action Click ...

                        SessionManager.instance
                            .addBooleanValueWithKey(PREF_SLIDE_OPEN, true);

                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TiGadgetsHomePage()),
                        );
                      },
                      child: Text(
                        "COMMENCER",
                        style: TextStyle(color: Colors.white),
                      ))),
            ),
          )),
    ],
  );
}
