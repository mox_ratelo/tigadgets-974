import 'package:Tigadgets_974/src/utils/globalVariable.dart';
import 'package:Tigadgets_974/src/utils/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Slide1(BuildContext context, PageController _pageController) {
  return Stack(
    children: <Widget>[
      Positioned(
        bottom: 0,
        left: 0,
        right: 0,
        child: Container(
          color: Color(0xFFe6e4f0),
          width: double.infinity,
          height: getScreenHeight(context),
        ),
      ),
      /*Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: Container(
            width: double.infinity,
            height: getScreenHeight(context) * .7,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/snazzy-image.png"),
                    fit: BoxFit.fitWidth)),
          ),
        ),*/
      Positioned(
        top: -100,
        left: 0,
        right: 0,
        child: Container(
          width: double.infinity,
          height: getScreenHeight(context) * .6,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/splash1Background.png"),
                  fit: BoxFit.fill)),
        ),
      ),
      Positioned(
        bottom: getScreenHeight(context) * .23,
        right: 0,
        left: 0,
        child: Container(
            margin: EdgeInsets.symmetric(horizontal: 50),
            height: getScreenHeight(context) * .2,
            child: Text(
                "Trouvez plus facilement vos Accessoires Mobiles & Gadgets",
                style: TextStyle(
                    fontSize: 24,
                    fontFamily: Style.montserratBold,
                    color: Style.titleColor),
                textAlign: TextAlign.center,
                strutStyle: StrutStyle(height: 2.5))),
      ),
      Center(
          child: Container(
        width: (getScreenWidth(context) * .5) + 15,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: Center(
                child: Image.asset(
                  "assets/images/img_ico1.png",
                  scale: 3,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
            ),
            Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: Center(
                child: Image.asset(
                  "assets/images/img_ico2.png",
                  scale: 3,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
            ),
            Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: Center(
                child: Image.asset(
                  "assets/images/img_ico3.png",
                  scale: 3,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
            ),
          ],
        ),
      )),
      Positioned(
          bottom: getScreenHeight(context) * .1,
          left: 0,
          right: 0,
          child: Container(
            width: double.infinity,
            child: Center(
              child: Container(
                  width: 150,
                  height: 50,
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: BorderSide(color: Colors.grey[400])),
                      elevation: 0,
                      color: scaffoldBackgroundGrey,
                      onPressed: () {
                        _pageController.nextPage(
                            duration: Duration(milliseconds: 600),
                            curve: Curves.fastOutSlowIn);
                      },
                      child: Text(
                        "SUIVANT",
                        style: TextStyle(color: Colors.black),
                      ))),
            ),
          )),
    ],
  );
}
