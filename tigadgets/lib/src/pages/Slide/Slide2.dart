import 'package:Tigadgets_974/src/utils/globalVariable.dart';
import 'package:Tigadgets_974/src/utils/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Slide2(BuildContext context, PageController _pageController) {
  return Stack(
    children: <Widget>[
      Positioned(
        bottom: 0,
        left: 0,
        right: 0,
        child: Container(
          color: Color(0xFFe6e4f0),
          width: double.infinity,
          height: getScreenHeight(context),
        ),
      ),
      Positioned(
        top: -50,
        left: 0,
        right: 0,
        child: Container(
          width: double.infinity,
          height: getScreenHeight(context) * .6,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/splash2Background.png"),
                  fit: BoxFit.fill)),
        ),
      ),
      Positioned(
        top: 20,
        left: 0,
        right: 0,
        child: Container(
          width: double.infinity,
          height: getScreenHeight(context) * .53,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/phone.png"),
                  fit: BoxFit.fitHeight)),
        ),
      ),
      Positioned(
        bottom: getScreenHeight(context) * .23,
        right: 0,
        left: 0,
        child: Container(
            margin: EdgeInsets.symmetric(horizontal: 50),
            height: getScreenHeight(context) * .2,
            child: Text(
              "Besoin d’aide ? posez nous vos questions",
              style: TextStyle(
                fontSize: 23,
                fontFamily: Style.montserratBold,
                color: Style.titleColor,
              ),
              textAlign: TextAlign.center,
              strutStyle: StrutStyle(height: 2.5),
            )),
      ),
      Positioned(
          bottom: getScreenHeight(context) * .1,
          left: 0,
          right: 0,
          child: Container(
            width: double.infinity,
            child: Center(
              child: Container(
                  width: 150,
                  height: 50,
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: BorderSide(color: Colors.grey[400])),
                      elevation: 0,
                      color: scaffoldBackgroundGrey,
                      onPressed: () {
                        _pageController.nextPage(
                            duration: Duration(milliseconds: 600),
                            curve: Curves.fastOutSlowIn);
                      },
                      child: Text(
                        "SUIVANT",
                        style: TextStyle(color: Colors.black),
                      ))),
            ),
          )),
    ],
  );
}
