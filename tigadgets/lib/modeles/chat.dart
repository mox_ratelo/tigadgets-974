class Chat {
  String userId;
  String userName;
  String message;

  Chat({this.message, this.userId, this.userName});

  factory Chat.fromJson(var data) => Chat(
        message: data["message"],
        userId: data['userId'],
        userName: data['userName'],
      );

  Map<String, String> toJSON() =>
      {"userId": userId, 'userName': userName, 'message': message};
}
